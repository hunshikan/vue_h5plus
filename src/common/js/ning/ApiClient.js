import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)
let apiClient = function (medthod, urllink, param, successCallback, errorCallback) {
  if (medthod === 'GET') {
    Vue.axios.get(urllink,
      {
        'headers': {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        'params': param
      }).then((response) => {
        if (response.status === 200) {
          if (successCallback) {
            successCallback(response.data)
          }
        }
      }).catch(function (error) {
        if (errorCallback) {
          errorCallback(error)
        }
      })
  } else if (medthod === 'POST') {
    Vue.axios.post(urllink, JSON.stringify(param), 
    {
    	'headers': {
	      'Accept': 'application/json',
	      'Content-Type': 'application/json'
	    }
    }).then(function (response) {
      if (response.status === 200) {
        if (successCallback) {
          successCallback(response.data)
        }
      }
    }).catch(function (error) {
      if (errorCallback) {
        errorCallback()
      }
    })
  } else if (medthod === 'PUT') {
    Vue.axios.put(urllink, JSON.stringify(param), {'headers': {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    }).then(function (response) {
      if (response.status === 200) {
        if (successCallback) {
          successCallback(response.data)
        }
      }
    }).catch(function (error) {
      if (errorCallback) {
        errorCallback()
      }
    })
  } else if (medthod === 'DELETE') {
    Vue.axios.delete(urllink,
    {
    	'headers':{
	      'Accept': 'application/json',
	      'Content-Type': 'application/json'
	    },
	    'params': param
    }).then(function (response) {
      if (response.status === 200) {
        if (successCallback) {
          successCallback(response.data)
        }
      }
    }).catch(function (error) {
      if (errorCallback) {
        errorCallback()
      }
    })
  }
}
export default apiClient

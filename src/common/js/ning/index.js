
import 'common/css/font-css.css'
import 'muse-ui/dist/muse-ui.css'

import Vue from 'vue'
import MuseUI from 'muse-ui' 
MuseUI.config({
 disableTouchRipple: true,
 disableFocusRipple: false
})
Vue.use(MuseUI)
//默认domReady事件
import domReady from './ready.js'
//默认plusReady事件
import plusReady from './plusReady.js'

module.exports = {
    domReady,
    plusReady
}